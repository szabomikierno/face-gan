def config():
	models_dir = 'models'

	return {
		'dir': {
			'encoder': 'encoder',
			'models': models_dir,

			'raw_images': 'raw_images',
			'aligned_images': 'aligned_images',
			'generated_images': 'generated_images',
			'latent_representations': 'latent_representations',
		},

		'vgg16': 'vgg16_zhang_perceptual.pkl',
		'model': 'encoder/stylegan2-ffhq-config-f.pkl',
		
		'available_methods': ['slow', 'fast'],
		'method': 'slow',
		'available_networks': {
			'default': {
				'name': 'default',
				'pkl': models_dir + '/' + 'generator_model-stylegan2-config-f.pkl', 
			},
			
			'european': {
				'name': 'european',
				'pkl': models_dir + '/' + 'generator_model-stylegan2-config-f.pkl', 
			}, 
			
			'asian': {
				'name': 'asian',
				'pkl': models_dir + '/' + 'generator_yellow-stylegan2-config-f.pkl', 
			}, 
			
			'asian_beauty': {
				'name': 'asian beauty',
				'pkl': models_dir + '/' + 'generator_star-stylegan2-config-f.pkl', 
			}, 
			
			'baby': {
				'name': 'baby',
				'pkl': models_dir + '/' + 'generator_baby-stylegan2-config-f.pkl', 
			}
		},

		'network': 'default',

		'iterations': 750, #100 - 1500
		'learning_rate': 0.1, #0.01 - 1
		'delete_logs': True
	}

