import os
import sys
from config import config
import shutil

c = config()
network = c['network']
networks = c['available_networks']
network = networks[network]
pkl = network['pkl']

if c['method'] == 'slow':

    #need to change these calls somehow
    #need to normalize and standardize params wth is this shit
    #also fast method saves to latent_representations while slow doesn't
    os.system(
        'python ' + c['dir']['encoder'] + '/project_images.py ' + ' ' +
        c['dir']['aligned_images'] + ' ' +
        c['dir']['generated_images'] + ' ' +
        '--vgg16-pkl ' + str(c['vgg16']) + ' ' +
        '--num-steps ' + str(c['iterations']) + ' ' +
        '--initial-learning-rate ' + str(c['learning_rate']) + ' ' +
        '--network-pkl ' + pkl + ' ' +
        '--video=False'
    )

    source_dir = c['dir']['generated_images']
    target_dir = c['dir']['latent_representations']
        
    file_names = os.listdir(source_dir)
        
    for file_name in file_names:
        shutil.move(os.path.join(source_dir, file_name), target_dir + '/' + file_name.split('.')[0] + '.npy')

else:
    os.system(
        'python ' + c['dir']['encoder'] + '/project_images.py ' + ' ' +
        c['dir']['aligned_images'] + ' ' +
        c['dir']['generated_images'] + ' ' +
        c['dir']['latent_representations'] + ' ' +
        '--image_size 1024 ' +
        '--batch_size 2 ' +
        '--vgg16-pkl ' + str(c['vgg16']) + ' ' +
        '--iterations ' + str(c['iterations']) + ' ' +
        '--lr ' + str(c['learning_rate']) + ' ' +
        '--network_pkl ' + pkl + ' ' +
        '--video=False'
    )