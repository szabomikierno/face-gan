import os
import math
import pickle
import imageio
import PIL.Image
import numpy as np
from PIL import Image
import tensorflow as tf
import moviepy.editor as mpy
import matplotlib.pyplot as plt
from IPython.display import clear_output
from moviepy.video.io.ffmpeg_writer import FFMPEG_VideoWriter
import dnnlib
from encoder import pretrained_networks
import dnnlib.tflib as tflib
import config

c = config()

fn = !ls latent_representations
pass
if delete_logs == True:
  clear_output()
display_sbs('generated_images/', 'aligned_images/', res = 512)

#@title #**(Optional) Download the generated vector and image** { display-mode: "form" }

download_vector = True #@param {type:"boolean"}
if download_vector == True:
  files.download("latent_representations/photo_01.npy")

download_picture = True #@param {type:"boolean"}
if download_picture == True:
  files.download("generated_images/photo_01.png")

#@markdown *The bottom checkmark will simply show the thumbnail of the generated image under this block.*
show_image = False #@param {type:"boolean"}
if show_image == True:
  img2 = PIL.Image.open('generated_images/photo_01.png')
  wpercent = (256/float(img2.size[0]))
  hsize = int((float(img2.size[1])*float(wpercent)))
  img2 = img2.resize((256,hsize), PIL.Image.LANCZOS)
  display(img2)

  #@title #**(Optional) Upload your vector** { display-mode: "form" }

#@markdown *This block is optional. If you have a saved vector, then you can upload it*
from google.colab import files
!rm -rf latent_representations
uploaded = files.upload()

for fn in uploaded.keys():
  print('User uploaded file "{name}" with length {length} bytes'.format(
      name=fn, length=len(uploaded[fn])))

!mkdir latent_representations
!mv $fn latent_representations/$fn
fn = [fn]
clear_output()


#@title #**Configure the generator** { display-mode: "form" }

#@markdown *Below select the network in which the feature vector was generated*
network = 'european' #@param ["default", "european", "asian", "asian beauty", "baby"]

if network == 'default':
  network_pkl = 'networks/' + model
else:
  other_networks = !ls networks/other
  if networks_urls[network][1] in other_networks:
    network_pkl = 'networks/other/' + networks_urls[network][1]
  else:
    network_url = networks_urls[network][0]
    try:
      !gdown $network_url
      network_name = networks_urls[network][1]
      !mv $network_name networks/other/$network_name
      network_pkl = 'networks/other/' + network_name
    except BaseException:
      network_pkl = 'networks/other/' + networks_urls[network][1]

tflib.init_tf()
with open(network_pkl, "rb") as f:
  generator_network, discriminator_network, Gs_network = pickle.load(f)

w_avg = Gs_network.get_var('dlatent_avg')
noise_vars = [var for name, var in Gs_network.components.synthesis.vars.items() if name.startswith('noise')]
Gs_syn_kwargs = dnnlib.EasyDict()
Gs_syn_kwargs.output_transform = dict(func=tflib.convert_images_to_uint8, nchw_to_nhwc=True)
Gs_syn_kwargs.randomize_noise = False
Gs_syn_kwargs.minibatch_size = 1
truncation_psi = 0.5
clear_output()


#@title #**Modify the face in photo** { run: "auto", display-mode: "form" }
from google.colab import files

v = np.load('latent_representations/'+fn[0])
v = np.array([v]) # если будет не похоже, то можно поэкспериментировать с (* truncation_psi)
!rm -rf results/$direction_name

#@markdown **Choose what you would like to change.:**
parameter = 'race_black' #@param ["age", "angle_horizontal", "angle_vertical", "beauty", "emotion_angry", "emotion_disgust", "emotion_easy", "emotion_fear", "emotion_happy", "emotion_sad", "emotion_surprise", "eye_distance", "eye_eyebrow_distance", "eye_ratio", "eyes_open", "face_shape", "gender", "glasses", "height", "lip_ratio", "mouth_open", "mouth_ratio", "nose_mouth_distance", "nose_ratio", "nose_tip", "race_black", "race_white", "race_yellow", "smile", "width"]
direction_file = parameter + '.npy'
direction_name = direction_file.split('.')[0]
#@markdown **Parameter intensity:**
intensity = 5 #@param {type:"slider", min:-20, max:20, step:0.2}
#@markdown *If you set the boost_intensity, then the intensity will increase 3 times.*
boost_intensity = True #@param {type:"boolean"}
if boost_intensity == True:
  intensity *= 3
coeffs = [intensity]
#@markdown **Image resolution:**
resolution = "256" #@param [128, 256, 512, 1024]
size = int(resolution), int(resolution)

move_latent_and_save(v, direction_file, coeffs, Gs_network, Gs_syn_kwargs)


#@title #**Animating a face modification** { display-mode: "form" }
from google.colab import files

v = np.load('latent_representations/'+fn[0])
v = np.array([v]) # если будет не похоже, то можно поэкспериментировать с (* truncation_psi)
!rm -rf results/$direction_name

#@markdown **Choose what you would like to change.:**
parameter = 'eyes_open' #@param ["age", "angle_horizontal", "angle_vertical", "beauty", "emotion_angry", "emotion_disgust", "emotion_easy", "emotion_fear", "emotion_happy", "emotion_sad", "emotion_surprise", "eye_distance", "eye_eyebrow_distance", "eye_ratio", "eyes_open", "face_shape", "gender", "glasses", "height", "lip_ratio", "mouth_open", "mouth_ratio", "nose_mouth_distance", "nose_ratio", "nose_tip", "race_black", "race_white", "race_yellow", "smile", "width"]
direction_file = parameter + '.npy'
direction_name = direction_file.split('.')[0]
#@markdown **Starting intensity:**
start_intensity = 0 #@param {type:"slider", min:-20, max:20, step:0.2}
#@markdown **Finishing intensity:**
finish_intensity = 10 #@param {type:"slider", min:-20, max:20, step:0.2}
#@markdown **Number of frames:**
frames = 40 #@param {type:"slider", min:10, max:100, step:1}

if finish_intensity > start_intensity:
  #@markdown *If you set the boost_intensity, then the intensity will increase 3 times.*
  boost_intensity = True #@param {type:"boolean"}
  if boost_intensity == True:
    start_intensity *= 3
    finish_intensity *= 3
  
  duration = abs(start_intensity)+abs(finish_intensity)
  steps = round(duration/frames,2)
  coeffs = []

  for i in range(int(start_intensity*100), int(finish_intensity*100), int(steps*100)):
    coeffs.append(i/100)
  #@markdown **Animation Resolution:**
  resolution = "256" #@param [128, 256, 512, 1024]
  size = int(resolution), int(resolution)

  move_latent_and_save(v, direction_file, coeffs, Gs_network, Gs_syn_kwargs)

  #@markdown **Add video Invert:**

  add_invert = True #@param {type:"boolean"}
  face_img = []
  img = os.listdir("results/"+direction_name)
  img.sort()
  for i in img:
    face_img.append(imageio.imread("results/" + direction_name + "/"+i))
  if add_invert == True:
    for j in reversed(face_img):
      face_img.append(j)
  face_img = np.array(face_img)
  imageio.mimsave("results/" + direction_name + "/" + direction_name + ".mp4", face_img)
  display(mpy.ipython_display("results/" + direction_name + "/" + direction_name + ".mp4", height=400, autoplay=1, loop=1))
else:
  print('Starting intensity should be less than the finish!')


 #@title #**Modify the face according to 3 parameters** { display-mode: "form" }
from google.colab import files

v = np.load('latent_representations/'+fn[0])
v = np.array([v]) # если будет не похоже, то можно поэкспериментировать с (* truncation_psi)
!rm -rf results/3param

#@markdown **Choose the first option:**
parameter1 = 'angle_horizontal' #@param ["age", "angle_horizontal", "angle_vertical", "beauty", "emotion_angry", "emotion_disgust", "emotion_easy", "emotion_fear", "emotion_happy", "emotion_sad", "emotion_surprise", "eye_distance", "eye_eyebrow_distance", "eye_ratio", "eyes_open", "face_shape", "gender", "glasses", "height", "lip_ratio", "mouth_open", "mouth_ratio", "nose_mouth_distance", "nose_ratio", "nose_tip", "race_black", "race_white", "race_yellow", "smile", "width"]
direction_file1 = np.load('latent_directions/' + parameter1 + '.npy')
intensity1 = 7 #@param {type:"slider", min:-20, max:20, step:0.2}
boost_intensity1 = True #@param {type:"boolean"}
if boost_intensity1 == True:
  intensity1 *= 3

#@markdown **Choose the second option:**
parameter2 = 'race_yellow' #@param ["age", "angle_horizontal", "angle_vertical", "beauty", "emotion_angry", "emotion_disgust", "emotion_easy", "emotion_fear", "emotion_happy", "emotion_sad", "emotion_surprise", "eye_distance", "eye_eyebrow_distance", "eye_ratio", "eyes_open", "face_shape", "gender", "glasses", "height", "lip_ratio", "mouth_open", "mouth_ratio", "nose_mouth_distance", "nose_ratio", "nose_tip", "race_black", "race_white", "race_yellow", "smile", "width"]
direction_file2 = np.load('latent_directions/' + parameter2 + '.npy')
intensity2 = 5 #@param {type:"slider", min:-20, max:20, step:0.2}
boost_intensity2 = True #@param {type:"boolean"}
if boost_intensity2 == True:
  intensity2 *= 3

#@markdown **Choose the third option:**
parameter3 = 'emotion_happy' #@param ["age", "angle_horizontal", "angle_vertical", "beauty", "emotion_angry", "emotion_disgust", "emotion_easy", "emotion_fear", "emotion_happy", "emotion_sad", "emotion_surprise", "eye_distance", "eye_eyebrow_distance", "eye_ratio", "eyes_open", "face_shape", "gender", "glasses", "height", "lip_ratio", "mouth_open", "mouth_ratio", "nose_mouth_distance", "nose_ratio", "nose_tip", "race_black", "race_white", "race_yellow", "smile", "width"]
direction_file3 = np.load('latent_directions/' + parameter3 + '.npy')
intensity3 = 4 #@param {type:"slider", min:-20, max:20, step:0.2}
boost_intensity3 = True #@param {type:"boolean"}
if boost_intensity3 == True:
  intensity3 *= 3

#@markdown **Image resolution:**
resolution = "256" #@param [128, 256, 512, 1024]
size = int(resolution), int(resolution)

def move_latent_and_save_3_param(latent_vector, direction_intensity, Gs_network, Gs_syn_kwargs):
    os.makedirs('results/3param', exist_ok=True)
    new_latent_vector = latent_vector.copy()
    new_latent_vector[0][:8] = (latent_vector[0] + direction_intensity)[:8]
    images = Gs_network.components.synthesis.run(new_latent_vector, **Gs_syn_kwargs)
    result = PIL.Image.fromarray(images[0], 'RGB')
    result.thumbnail(size, PIL.Image.ANTIALIAS)
    result.save('results/3param/result_3param.png')
    return result

direction_intensity = (direction_file1*intensity1) + (direction_file2*intensity2) + (direction_file3*intensity3)
move_latent_and_save_3_param(v, direction_intensity, Gs_network, Gs_syn_kwargs)


#@title #**Animating face modification (3 parameters)** { display-mode: "form" }
from google.colab import files

v = np.load('latent_representations/'+fn[0])
v = np.array([v]) # если будет не похоже, то можно поэкспериментировать с (* truncation_psi)
!rm -rf results/3param

#@markdown **Number of frames:**
frames = 40 #@param {type:"slider", min:10, max:100, step:1}

#@markdown **Choose the first option:**
parameter1 = 'angle_horizontal' #@param ["age", "angle_horizontal", "angle_vertical", "beauty", "emotion_angry", "emotion_disgust", "emotion_easy", "emotion_fear", "emotion_happy", "emotion_sad", "emotion_surprise", "eye_distance", "eye_eyebrow_distance", "eye_ratio", "eyes_open", "face_shape", "gender", "glasses", "height", "lip_ratio", "mouth_open", "mouth_ratio", "nose_mouth_distance", "nose_ratio", "nose_tip", "race_black", "race_white", "race_yellow", "smile", "width"]
direction_file1 = np.load('latent_directions/' + parameter1 + '.npy')
intensity1 = 10 #@param {type:"slider", min:-20, max:20, step:0.2}
if intensity1 == 0: intensity1 += 0.001
boost_intensity1 = True #@param {type:"boolean"}
if boost_intensity1 == True:
  intensity1 *= 3
coeffs1 = []
for i in range(0, frames):
  coeffs1.append(round((i*intensity1)/frames,3))

#@markdown **Choose the second option:**
parameter2 = 'race_yellow' #@param ["age", "angle_horizontal", "angle_vertical", "beauty", "emotion_angry", "emotion_disgust", "emotion_easy", "emotion_fear", "emotion_happy", "emotion_sad", "emotion_surprise", "eye_distance", "eye_eyebrow_distance", "eye_ratio", "eyes_open", "face_shape", "gender", "glasses", "height", "lip_ratio", "mouth_open", "mouth_ratio", "nose_mouth_distance", "nose_ratio", "nose_tip", "race_black", "race_white", "race_yellow", "smile", "width"]
direction_file2 = np.load('latent_directions/' + parameter2 + '.npy')
intensity2 = 8 #@param {type:"slider", min:-20, max:20, step:0.2}
if intensity2 == 0: intensity2 += 0.001
boost_intensity2 = True #@param {type:"boolean"}
if boost_intensity2 == True:
  intensity2 *= 3
coeffs2 = []
for i in range(0, frames):
  coeffs2.append(round((i*intensity2)/frames,3))

#@markdown **Choose the third option:**
parameter3 = 'emotion_happy' #@param ["age", "angle_horizontal", "angle_vertical", "beauty", "emotion_angry", "emotion_disgust", "emotion_easy", "emotion_fear", "emotion_happy", "emotion_sad", "emotion_surprise", "eye_distance", "eye_eyebrow_distance", "eye_ratio", "eyes_open", "face_shape", "gender", "glasses", "height", "lip_ratio", "mouth_open", "mouth_ratio", "nose_mouth_distance", "nose_ratio", "nose_tip", "race_black", "race_white", "race_yellow", "smile", "width"]
direction_file3 = np.load('latent_directions/' + parameter3 + '.npy')
intensity3 = 7 #@param {type:"slider", min:-20, max:20, step:0.2}
if intensity3 == 0: intensity3 += 0.001
boost_intensity3 = True #@param {type:"boolean"}
if boost_intensity3 == True:
  intensity3 *= 3
coeffs3 = []
for i in range(0, frames):
  coeffs3.append(round((i*intensity3)/frames,3))

#@markdown **Animation Resolution:**
resolution = "256" #@param [128, 256, 512, 1024]
size = int(resolution), int(resolution)

def move_latent_and_save_3_param(latent_vector, direction_intensity, frame_num, Gs_network, Gs_syn_kwargs):
    os.makedirs('results/3param', exist_ok=True)
    new_latent_vector = latent_vector.copy()
    new_latent_vector[0][:8] = (latent_vector[0] + direction_intensity)[:8]
    images = Gs_network.components.synthesis.run(new_latent_vector, **Gs_syn_kwargs)
    result = PIL.Image.fromarray(images[0], 'RGB')
    result.thumbnail(size, PIL.Image.ANTIALIAS)
    result.save('results/3param/result_(' + str(frame_num+1000) + ')_3param.png')
    return result

for i in range(frames):
  direction_intensity1 = direction_file1 * coeffs1[i]
  direction_intensity2 = direction_file2 * coeffs2[i]
  direction_intensity3 = direction_file3 * coeffs3[i]
  direction_intensity = direction_intensity1 + direction_intensity2 + direction_intensity3
  move_latent_and_save_3_param(v, direction_intensity, i, Gs_network, Gs_syn_kwargs)
  #clear_output()
  #print('Сгенерированно ' + str(i) + ' фото из ' + str(frames))

#@markdown **Add video Invert:**

add_invert = True #@param {type:"boolean"}
face_img = []
img = os.listdir("results/3param")
img.sort()
clear_output()
print('Animation is created. Please wait.')
for i in img:
  face_img.append(imageio.imread("results/3param/"+i))
if add_invert == True:
  for j in reversed(face_img):
    face_img.append(j)
face_img = np.array(face_img)
imageio.mimsave("results/3param/3param.mp4", face_img)
display(mpy.ipython_display("results/3param/3param.mp4", height=400, autoplay=1, loop=1))


