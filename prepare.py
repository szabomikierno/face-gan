import os
import PIL.Image
from config import config

c = config()

for image_path in os.listdir(c['dir']['raw_images']):
	image_full_path = c['dir']['raw_images'] + '/' + image_path
	img = PIL.Image.open(image_full_path)
	wpercent = (256 / float(img.size[0]))
	hsize = int((float(img.size[1]) * float(wpercent)))
	img = img.resize((256,hsize), PIL.Image.LANCZOS)
	
	#CHANGE THIS AFTER EXTRACTING ENCODER ALIGN TO FUNCTION
	os.system(
		'python ' + 
		c['dir']['encoder'] + '/align_images.py ' + 
		c['dir']['raw_images'] + '/ ' + 
		c['dir']['aligned_images'] + '/'
	)